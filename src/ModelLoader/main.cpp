#include "ModelLoader/ModelLoader.h"

int main(int argc, char **argv) {
    //#### ROS-Initialisation ####
    ros::init(argc, argv, "model_loader");
    ROS_INFO("Starting ModelLoader-node with node name %s", ros::this_node::getName().c_str());
    ros::NodeHandle node;
    ros::NodeHandle nodeLocal("~");

    //#### Check whether a scan list is provided or not #####
    if(argc != 3) {
        ROS_ERROR("Insufficient args. A list of scan files and a normalization flag must be given.");
        exit(-1);
    }

    // Create a ModelLoader instance
    ModelLoader modelLoader(&node, &nodeLocal, argv[1]);

    // Publish models in the scan list. You may save the pts as ply.
    modelLoader.publishScanData(true, argv[2], 300, 900, true, 0.3, ply::BINARY);

    while(ros::ok())
    {
        ros::spinOnce();
    }

    return 0;
}
