/*
 *		PrImageProcessing Library
 *
 *		Ryo Kurazume kurazume@cvl.iis.u-tokyo.ac.jp
 *						Tokyo University
 *						2000.10.25
 */

#include "stdafx.h"
#include <fstream>
#include "PrGlobalDefn.h"

#include "matrix/PrVector3.h"
#include "matrix/PrVector6.h"
#include "matrix/PrMatrix6.h"
#include "matrix/PrQuaternion.h"
#include "matrix/PrTransform.h"

//#include "image/PrBitmap.h"
#include "image/PrPly.h"
#include <map>


PrPly::PrPly()
{
	nvertex_ = -1;
	nmesh_ = -1;
	vertex_ = NULL;
	attr_   = NULL;
	index_  = NULL;
	normal_ = NULL;
	status_ = NULL;
	tvertex_  = NULL;
	tattr_  = NULL;
	TransformMatrix = FALSE;
	R.diagonal(1,1,1);
	T.values(0,0,0);
	maxx = maxy = maxz = minx = miny = minz = 0;
	avg = 0;
	COLORTYPE = MY_RGBA;
	for(int i=0; i<MAXTEXS; i++) texture_[i] = false;
	IntrinsicMatrix = FALSE;
	A.diagonal(1,1,1);
}

PrPly::PrPly(const PrPly &sPly)
{
	int i, j;

	nvertex_ = sPly.nvertex_;
	nmesh_ = sPly.nmesh_;

	vertex_ = new ply_vertex[nvertex_];
	attr_   = new ply_attr[nvertex_];
	index_  = new ply_index[nmesh_];
	normal_ = new ply_vector[nmesh_];
	status_ = new BYTE[nmesh_];
	if(sPly.tvertex_) tvertex_ = new tex_vertex[nvertex_];
	else tvertex_ = NULL;
	if(sPly.tattr_) tattr_ = new tex_attr[nmesh_];
	else tattr_ = NULL;

	tpos_ = sPly.tpos_;
	for(i = 0; i < nvertex_; ++i){
		vertex_[i].p = sPly.vertex_[i].p;
		attr_[i].confidence_ = sPly.attr_[i].confidence_;
		attr_[i].intensity_  = sPly.attr_[i].intensity_;
		if(sPly.tvertex_){
			for(j = 0; j < MAXTEXS; ++j){
				tvertex_[i].u[j] = sPly.tvertex_[i].u[j];
				tvertex_[i].v[j] = sPly.tvertex_[i].v[j];
			}
		}
	}
	for(i = 0; i < nmesh_; ++i){
		index_[i].nindex_ = sPly.index_[i].nindex_;
		index_[i].index0_ = sPly.index_[i].index0_;
		index_[i].index1_ = sPly.index_[i].index1_;
		index_[i].index2_ = sPly.index_[i].index2_;
		normal_[i].p = sPly.normal_[i].p;
		status_[i] = sPly.status_[i];
		if(sPly.tattr_){
			for(j = 0; j < MAXTEXS; ++j){
				tattr_[i].number[j] = sPly.tattr_[i].number[j];
				tattr_[i].direction[j] = sPly.tattr_[i].direction[j];
				tattr_[i].depth[j] = sPly.tattr_[i].depth[j];
			}
		}
	}

	for(i=0; i<MAXTEXS; i++) texture_[i] = sPly.texture_[i];

	center_= sPly.center_;
	maxx = sPly.maxx;
	minx = sPly.minx;
	maxy = sPly.maxy;
	miny = sPly.miny;
	maxz = sPly.maxz;
	minz = sPly.minz;
	TransformMatrix = sPly.TransformMatrix;
	R = sPly.R;
	T = sPly.T;
	avg = sPly.avg;
	COLORTYPE = sPly.COLORTYPE;
	IntrinsicMatrix = sPly.IntrinsicMatrix;
	A = sPly.A;
}

PrPly::PrPly(const PrPly &sPly, int t)
{
	int i, j;
	int*		_flags = new int[sPly.nvertex_];
	int*		_mflags = new int[sPly.nmesh_];
	::memset((char*)_flags, 0, sizeof(int) * sPly.nvertex_);	
	::memset((char*)_mflags, 0, sizeof(int) * sPly.nmesh_);	
	int			_idx = 0;

	for(i=0;i<sPly.nmesh_;i++){
		if(sPly.tattr_[i].number[t]){
			++_flags[sPly.index_[i].index0_];
			++_flags[sPly.index_[i].index1_];
			++_flags[sPly.index_[i].index2_];
			++_mflags[i];

			++_idx;
		}
	}

	int		_rvnum = 0;
	for(i = 0; i < sPly.nvertex_; ++i){
		if(_flags[i]){
			_flags[i] = _rvnum++;
		}
		else
			_flags[i] = -1;
	}

	vertex_ = new ply_vertex[_rvnum];
	tvertex_ = new tex_vertex[_rvnum];
	attr_   = new ply_attr[_rvnum];

	int			_vnum = 0;
	for(i = 0; i < sPly.nvertex_; ++i){
		if(_flags[i] >= 0){
			vertex_[_vnum].p = sPly.vertex_[i].p;
			tvertex_[_vnum].u[0] = sPly.tvertex_[i].u[t];
			tvertex_[_vnum].v[0] = sPly.tvertex_[i].v[t];
			attr_[_vnum].confidence_ = sPly.attr_[i].confidence_;
			attr_[_vnum].intensity_  = sPly.attr_[i].intensity_;
			_vnum++;
		}
	}

	index_ = new ply_index[_idx];
	normal_ = new ply_vector[_idx];
	status_ = new BYTE[_idx];
	tattr_ = new tex_attr[_idx];
	int			_inum = 0;
	for(i = 0; i < sPly.nmesh_; i++){
		if(_mflags[i]){
			index_[_inum].index0_ = _flags[sPly.index_[i].index0_];
			index_[_inum].index1_ = _flags[sPly.index_[i].index1_];
			index_[_inum].index2_ = _flags[sPly.index_[i].index2_];
			normal_[_inum].p = sPly.normal_[i].p;
			status_[_inum] = sPly.status_[i];
			if(sPly.tattr_){
				for(j = 0; j < MAXTEXS; ++j){
					tattr_[_inum].number[j] = sPly.tattr_[i].number[j];
					tattr_[_inum].direction[j] = sPly.tattr_[i].direction[j];
					tattr_[_inum].depth[j] = sPly.tattr_[i].depth[j];
				}
			}
			_inum++;
		}
	}

	nvertex_ = _vnum;
	nmesh_   = _inum;

	delete []	_mflags;
	delete []	_flags;

	texture_[0] = TRUE;

	center_= sPly.center_;
	maxx = sPly.maxx;
	minx = sPly.minx;
	maxy = sPly.maxy;
	miny = sPly.miny;
	maxz = sPly.maxz;
	minz = sPly.minz;
	TransformMatrix = sPly.TransformMatrix;
	R = sPly.R;
	T = sPly.T;
	avg = sPly.avg;
	COLORTYPE = sPly.COLORTYPE;
	IntrinsicMatrix = sPly.IntrinsicMatrix;
	A = sPly.A;
}

PrPly::PrPly(int _nvertex, int _nmesh)
{
	nvertex_ = _nvertex;
	nmesh_ = _nmesh;

	vertex_ = new ply_vertex[nvertex_];
	attr_   = new ply_attr[nvertex_];
	index_  = new ply_index[nmesh_];
	normal_ = new ply_vector[nmesh_];
	status_ = new BYTE[nmesh_];
	tvertex_  = NULL;
	tattr_  = NULL;
	for(int i=0; i<MAXTEXS; i++) texture_[i] = FALSE;

	TransformMatrix = FALSE;
	R.diagonal(1,1,1);
	T.values(0,0,0);
	maxx = maxy = maxz = minx = miny = minz = 0;
	avg = 0;
	COLORTYPE = MY_RGBA;
	IntrinsicMatrix = FALSE;
	A.diagonal(1,1,1);
}

PrPly::~PrPly()
{
	if(vertex_) delete [] vertex_;
	if(attr_)   delete [] attr_;
	if(index_)  delete [] index_;
	if(normal_) delete [] normal_;
	if(status_) delete [] status_;
	if(tvertex_) delete[] tvertex_;
	if(tattr_) delete[] tattr_;
}

BOOL PrPly::LoadPlyFile(char* lpszPathName, bool CCW, bool HEADERONLY, bool RECOVER, BOOL CENTERING)
{
	// ファイルのオープン
	int i;
	char buf[256], dummy1[256], dummy2[256];
	//LPTSTR buf, dummy1, dummy2;
	int type = 0;
	int column = 0;
	Float dummyf;
	Float MtoMM = 1000.0f;
	bool USE_CONFIDENCE = false;
	bool USE_INTENSITY = false;
	bool USE_COLOR = false;
	bool USE_RGB = false;

	std::ifstream	_is(lpszPathName, std::ios::in | std::ios::_Nocreate | std::ios::binary);

	if(!_is){
		AfxMessageBox(_T("File cannot open"),MB_OK);
//		CString s(lpszPathName);
//		AfxMessageBox(s,MB_OK);
		return FALSE;
	}
	
	while(1){
		long pos;
		pos = _is.tellg();
		_is.getline(buf, 256);
    
		if (strstr(buf, "element vertex") != NULL){
			_is.seekg(pos);
			_is >> dummy1 >> dummy2 >> nvertex_;
		}

		if (strstr(buf, "element face") != NULL){
			_is.seekg(pos);
			_is >> dummy1 >> dummy2 >> nmesh_;
		}

		if (strstr(buf, "end_header") != NULL)
			break;

		if ((strstr(buf, "format binary_big_endian 1.0") != NULL) || (strstr(buf, "format binary_big_endian") != NULL)) {
			AfxMessageBox(_T("Cannot treat binary_big_endian \n Sorry ..."),MB_OK);
			_is.close();
			return FALSE;
		}

		if ((strstr(buf, "format binary_little_endian 1.0") != NULL) || (strstr(buf, "format binary_little_endian") != NULL))
			type = BINARY;

		if (strstr(buf, "format ascii 1.0") != NULL)
			type = ASCII;

		if (strstr(buf, "comment with matrix") != NULL) {
			_is >> dummy1 >> R.elementAt(0,0) >> R.elementAt(1,0) >> R.elementAt(2,0) >> dummyf;
			_is >> dummy1 >> R.elementAt(0,1) >> R.elementAt(1,1) >> R.elementAt(2,1) >> dummyf;
			_is >> dummy1 >> R.elementAt(0,2) >> R.elementAt(1,2) >> R.elementAt(2,2) >> dummyf;
			_is >> dummy1 >> T.elementAt(0) >> T.elementAt(1) >> T.elementAt(2) >> dummyf;
			TransformMatrix = TRUE;
		}

		if (strstr(buf, "comment with intrinsic matrix") != NULL) {
			_is >> dummy1 >> A.elementAt(0,0) >> A.elementAt(0,1) >> A.elementAt(0,2);
			_is >> dummy1 >> A.elementAt(1,0) >> A.elementAt(1,1) >> A.elementAt(1,2);
			_is >> dummy1 >> A.elementAt(2,0) >> A.elementAt(2,1) >> A.elementAt(2,2);
			IntrinsicMatrix = TRUE;
		}

		if (strstr(buf, "property float confidence") != NULL) 
			USE_CONFIDENCE = true;

		if (strstr(buf, "property float intensity") != NULL) 
			USE_INTENSITY = true;

		if (strstr(buf, "property uchar blue") != NULL){
			if(!USE_COLOR) COLORTYPE = MY_BGRA;
			USE_COLOR = true;
		}

		if (strstr(buf, "property uchar green") != NULL)
			USE_COLOR = true;

		if (strstr(buf, "property uchar red") != NULL){
			if(!USE_COLOR) COLORTYPE = MY_RGBA;
			USE_COLOR = true;
		}

		if (strstr(buf, "property uchar alpha") != NULL)
			USE_COLOR = true;
	}

	if(HEADERONLY) {
		_is.close();
		return TRUE;
	}

	if(vertex_) delete[] vertex_;
	if(attr_) delete[] attr_;
	if(index_) delete[] index_;
	if(normal_) delete[] normal_;
	if(status_) delete[] status_;
	if(tvertex_) delete[] tvertex_;
	if(tattr_) delete[] tattr_;

	vertex_ = new ply_vertex[nvertex_];
	attr_   = new ply_attr[nvertex_];
	index_  = new ply_index[nmesh_];
	normal_ = new ply_vector[nmesh_];
	status_ = new BYTE[nmesh_];
	tvertex_  = NULL;
	tattr_  = NULL;

	int _nv(0), _nm(0);

	if(type == BINARY){
		float vinfo[5];
		int bsize = 3*sizeof(float);
		if(USE_CONFIDENCE) bsize += sizeof(float);
		if(USE_INTENSITY)  bsize += sizeof(float);
		if(USE_COLOR)  bsize += sizeof(float);
		for(i = 0; i < nvertex_; ++i){
			if(!_is.good()) break;
			_is.read((char*)&vinfo,bsize);			
			vertex_[i].p.values(vinfo[0],vinfo[1],vinfo[2]);
			if(USE_CONFIDENCE) attr_[i].confidence_ = vinfo[3];
				else attr_[i].confidence_ = 0.0;
			if(USE_INTENSITY)  attr_[i].intensity_  = vinfo[4];
				else attr_[i].intensity_ = 0.0;
			if(USE_COLOR) {
				switch(COLORTYPE){
				case MY_RGBA:
					attr_[i].color_.red    = *((char*)(vinfo+4)+0);
					attr_[i].color_.green  = *((char*)(vinfo+4)+1);
					attr_[i].color_.blue   = *((char*)(vinfo+4)+2);
					attr_[i].color_.alpha  = *((char*)(vinfo+4)+3);
					break;
				case MY_BGRA:
					attr_[i].color_.blue   = *((char*)(vinfo+4)+0);
					attr_[i].color_.green  = *((char*)(vinfo+4)+1);
					attr_[i].color_.red    = *((char*)(vinfo+4)+2);
					attr_[i].color_.alpha  = *((char*)(vinfo+4)+3);
					break;
				}
			} else attr_[i].intensity_ = 0.0;
			_nv++;
		}
		for(i = 0; i < nmesh_; ++i){
			if(!_is.good()) break;
			_is.read((char*)&index_[i].nindex_,1);
			_is.read((char*)&index_[i].index0_,12);
			status_[i] = SHOW;
			_nm++;
		}
	} else {
		for(i = 0; i < nvertex_; ++i){
			if(!_is.good()) break;
			_is >> vertex_[i].p.elementAt(0);
			_is >> vertex_[i].p.elementAt(1);
			_is >> vertex_[i].p.elementAt(2);
			if(USE_CONFIDENCE) _is >> attr_[i].confidence_;
				else attr_[i].confidence_ = 0.0;
			if(USE_INTENSITY) _is >> attr_[i].intensity_;
				else attr_[i].intensity_ = 0.0;
			if(USE_COLOR) {
				int r,g,b,a;
				switch(COLORTYPE){
				case MY_RGBA:
					_is >> r; attr_[i].color_.red = r;
					_is >> g; attr_[i].color_.green = g;
					_is >> b; attr_[i].color_.blue = b;
					_is >> a; attr_[i].color_.alpha = a;
					break;
				case MY_BGRA:
					_is >> b; attr_[i].color_.blue = b;
					_is >> g; attr_[i].color_.green = g;
					_is >> r; attr_[i].color_.red = r;
					_is >> a; attr_[i].color_.alpha = a;
					break;
				}
			} else attr_[i].intensity_ = 0.0;
			_nv++;
		}
		for(i = 0; i < nmesh_; ++i){
			if(!_is.good()) break;
			_is >> index_[i].nindex_;
			_is >> index_[i].index0_;
			_is >> index_[i].index1_;
			_is >> index_[i].index2_;
			status_[i] = SHOW;
			_nm++;
		}
	}

	_is.close();

	for(i=0; i<MAXTEXS; i++) texture_[i] = FALSE;

	if(nvertex_ != _nv || nmesh_ != _nm){
		CString str;
		str.Format(_T("This file is broken\nNumber of vertex or mesh is wrong\nvertex %d (%d)  mesh %d (%d)"),_nv,nvertex_,_nm,nmesh_);
		AfxMessageBox(str,MB_OK);

		if(vertex_) delete[] vertex_;
		if(attr_) delete[] attr_;
		if(index_) delete[] index_;
		if(normal_) delete[] normal_;
		if(status_) delete[] status_;
		if(tvertex_) delete[] tvertex_;
		if(tattr_) delete[] tattr_;

		vertex_ = NULL;
		attr_   = NULL;
		index_  = NULL;
		normal_ = NULL;
		status_ = NULL;
		tvertex_  = NULL;
		tattr_  = NULL;

		return FALSE;
	}

	if(TransformMatrix && RECOVER){
		if(AfxMessageBox(_T("変換マトリックスを検出しました\n元の座標系に戻しますか？"),MB_YESNO) == IDYES){
			for(i = 0; i < nvertex_; ++i){
				vertex_[i].p = ~R * (vertex_[i].p - T);
//				vertex_[i].p[0] *= -1.0;
			}
			R.diagonal(1,1,1);
			T.values(0,0,0);
			TransformMatrix = false;
		}
	}

	CalcAverage();
	CalcCenter();
	CalcScale();
	CalcSurfaceArea();
	CalcVolume();
	CalcNormal(CCW);

	if(CENTERING) {
		for(i = 0; i < nvertex_; ++i) vertex_[i].p -= center_;
	}

	Float max = -MAXFLOAT;
	Float min = MAXFLOAT;
	Float ref;
	bool CyraCGP = false;
	for(i=0; i<nvertex_; i++){
		if(max < attr_[i].confidence_) max = attr_[i].confidence_;
		if(min > attr_[i].confidence_) min = attr_[i].confidence_;
	}

	if(max == 2048.0 || min == -2048.0) CyraCGP = true;
	if(max == 0 && min == 0) max = 1.0;
	for(i=0; i<nvertex_; i++){
		ref = attr_[i].confidence_;
		if(ref==0.0 && CyraCGP)	ref = min;
		attr_[i].confidence_ = (ref-min)/(max-min);
	}

/*
	max = -MAXFLOAT;
	min = MAXFLOAT;
	for(i=0; i<nvertex_; i++){
		if(max < attr_[i].intensity_) max = attr_[i].intensity_;
		if(min > attr_[i].intensity_) min = attr_[i].intensity_;
	}

	if(max == 2048.0 || min == -2048.0) CyraCGP = true;
	for(i=0; i<nvertex_; i++){
		ref = attr_[i].intensity_;
		if(ref==0.0 && CyraCGP)	ref = min;
		attr_[i].intensity_ = (ref-min)/(max-min);
	}
*/
	// this code is only for sagawa's reflectance merging
//	CalcReflectanceEdges(0.07,CyraCGP);

	return TRUE;
}

BOOL PrPly::LoadPlyFileAsSMF(char* lpszPathName, bool CCW, BOOL CENTERING)
{
	// ファイルのオープン
	int i;
	ply_vertex*	_vertex;
	ply_index*	_index;
	ply_vector*	_normal;
	int			_nvertex = 0;
	int			_nmesh = 0;
	int			_nnormal = 0;
	int			max_nvertex = 2000000;
	int			max_nmesh   = 4000000;
	char		ch , buf[256];

	_vertex = new ply_vertex[max_nvertex];
	_index = new ply_index[max_nmesh];
	_normal = new ply_vector[max_nmesh];

	std::ifstream	_is(lpszPathName, std::ios::in | std::ios::_Nocreate | std::ios::binary);

	if(!_is){
		AfxMessageBox(_T("File cannot open"),MB_OK);
		return FALSE;
	}
	
	while(!_is.eof()){
		_is.getline(buf, 256);
    
		if(buf[0] == 'v') {
#ifdef PR_DOUBLE_PRECISION
			sscanf_s(buf,"%c %lf %lf %lf",&ch,
				&_vertex[_nvertex].p.elementAt(0),
				&_vertex[_nvertex].p.elementAt(1),
				&_vertex[_nvertex].p.elementAt(2));
#else
			sscanf_s(buf,"%c %f %f %f",&ch,
				&_vertex[_nvertex].p.elementAt(0),
				&_vertex[_nvertex].p.elementAt(1),
				&_vertex[_nvertex].p.elementAt(2));
#endif
			if(_nvertex ++ == max_nvertex){
				AfxMessageBox(_T("Too many vertexes"),MB_OK);
				delete[] _vertex;
				delete[] _index;
				delete[] _normal;
				return false;
			}
		}

		if(buf[0] == 'f') {
			sscanf_s(buf,"%c %d %d %d",&ch,
				&_index[_nmesh].index0_,
				&_index[_nmesh].index1_,
				&_index[_nmesh].index2_);
			if(_nmesh ++ == max_nmesh){
				AfxMessageBox(_T("Too many faces"),MB_OK);
				delete[] _vertex;
				delete[] _index;
				delete[] _normal;
				return false;
			}
		}

		if(buf[0] == 'n') {
#ifdef PR_DOUBLE_PRECISION
			sscanf_s(buf,"%c %lf %lf %lf",&ch,
				&_normal[_nnormal].p.elementAt(0),
				&_normal[_nnormal].p.elementAt(1),
				&_normal[_nnormal].p.elementAt(2));
#else
			sscanf_s(buf,"%c %lf %lf %lf",&ch,
				&_normal[_nnormal].p.elementAt(0),
				&_normal[_nnormal].p.elementAt(1),
				&_normal[_nnormal].p.elementAt(2));
#endif
			if(_nnormal ++ == max_nmesh){
				AfxMessageBox(_T("Too many normals"),MB_OK);
				delete[] _vertex;
				delete[] _index;
				delete[] _normal;
				return false;
			}
		}
	}

	_is.close();

	if(vertex_) delete[] vertex_;
	if(attr_) delete[] attr_;
	if(index_) delete[] index_;
	if(normal_) delete[] normal_;
	if(status_) delete[] status_;
	if(tvertex_) delete[] tvertex_;
	if(tattr_) delete[] tattr_;

	nvertex_ = _nvertex;
	nmesh_ = _nmesh;

	vertex_ = new ply_vertex[nvertex_];
	attr_   = new ply_attr[nvertex_];
	index_  = new ply_index[nmesh_];
	normal_ = new ply_vector[nmesh_];
	status_ = new BYTE[nmesh_];
	tvertex_  = NULL;
	tattr_  = NULL;

	for(i = 0; i < nvertex_; ++i){
		vertex_[i].p.elementAt(0) = _vertex[i].p.elementAt(0);
		vertex_[i].p.elementAt(1) = _vertex[i].p.elementAt(1);
		vertex_[i].p.elementAt(2) = _vertex[i].p.elementAt(2);
		attr_[i].confidence_ = 0.0;
		attr_[i].intensity_ = 0.0;
	}
	for(i = 0; i < nmesh_; ++i){
		index_[i].nindex_ = 3;
		index_[i].index0_ = _index[i].index0_ - 1;
		index_[i].index1_ = _index[i].index1_ - 1;
		index_[i].index2_ = _index[i].index2_ - 1;
		normal_[i].p.elementAt(0) = _normal[i].p.elementAt(0);
		normal_[i].p.elementAt(1) = _normal[i].p.elementAt(1);
		normal_[i].p.elementAt(2) = _normal[i].p.elementAt(2);
		status_[i] = SHOW;
	}

	delete[] _vertex;
	delete[] _index;
	delete[] _normal;

	for(i=0; i<MAXTEXS; i++) texture_[i] = FALSE;

	CalcCenter();
	CalcScale();
	CalcNormal(CCW);

	return TRUE;
}

BOOL PrPly::LoadPlyFileAsOBJ(char* lpszPathName, bool CCW, BOOL CENTERING)
{
	// ファイルのオープン
	int i;
	int			_nvertex = 0;
	int			_nmesh = 0;
	int			_nnormal = 0;
	int			max_nvertex = 2000000;
	int			max_nmesh   = 4000000;
	char		ch , buf[256];
	int			index0_,index1_,index2_,index3_;

	std::ifstream	_is(lpszPathName, std::ios::in | std::ios::_Nocreate | std::ios::binary);

	if(!_is){
		AfxMessageBox(_T("File cannot open"),MB_OK);
		return FALSE;
	}

	ply_vertex**	_pvertex = new ply_vertex*[max_nvertex];
	ply_index**		_pindex = new ply_index*[max_nmesh];
	ply_vertex**	_pnormal = new ply_vertex*[max_nmesh];

	_pvertex[_nvertex++] = new ply_vertex; // obj は 1 から始まる

	while(!_is.eof()){
		_is.getline(buf, 256);
    
		if(buf[0] == 'v') {
			_pvertex[_nvertex] = new ply_vertex;
#ifdef PR_DOUBLE_PRECISION
			sscanf_s(buf,"%c %lf %lf %lf",&ch,
				&_pvertex[_nvertex]->p.elementAt(0),
				&_pvertex[_nvertex]->p.elementAt(1),
				&_pvertex[_nvertex]->p.elementAt(2));
#else
			sscanf_s(buf,"%c %f %f %f",&ch,
				&_pvertex[_nvertex]->p.elementAt(0),
				&_pvertex[_nvertex]->p.elementAt(1),
				&_pvertex[_nvertex]->p.elementAt(2));
#endif
			_pvertex[_nvertex]->p *= 0.001;
			if(_nvertex ++ == max_nvertex){
				AfxMessageBox(_T("Too many vertexes"),MB_OK);
				return false;
			}
		}

		if(buf[0] == 'f') {
			index3_ = 0;
			sscanf_s(buf,"%c %d %d %d %d",&ch,
				&index0_, &index1_, &index2_, &index3_);
			if(index3_ == 0){
				_pindex[_nmesh] = new ply_index;
				_pindex[_nmesh]->index0_ = index0_;
				_pindex[_nmesh]->index1_ = index1_;
				_pindex[_nmesh]->index2_ = index2_;
				if(_nmesh ++ == max_nmesh){
					AfxMessageBox(_T("Too many faces"),MB_OK);
					return false;
				}
			} else {
				Float len1, len2;
				len1 = (_pvertex[index0_]->p - _pvertex[index2_]->p).magnitude();
				len2 = (_pvertex[index1_]->p - _pvertex[index3_]->p).magnitude();
				if(len1 < len2){
					_pindex[_nmesh] = new ply_index;
					_pindex[_nmesh]->index0_ = index0_;
					_pindex[_nmesh]->index1_ = index1_;
					_pindex[_nmesh]->index2_ = index2_;
					if(_nmesh ++ == max_nmesh){
						AfxMessageBox(_T("Too many faces"),MB_OK);
						return false;
					}

					_pindex[_nmesh] = new ply_index;
					_pindex[_nmesh]->index0_ = index0_;
					_pindex[_nmesh]->index1_ = index2_;
					_pindex[_nmesh]->index2_ = index3_;
					if(_nmesh ++ == max_nmesh){
						AfxMessageBox(_T("Too many faces"),MB_OK);
						return false;
					}
				} else {
					_pindex[_nmesh] = new ply_index;
					_pindex[_nmesh]->index0_ = index0_;
					_pindex[_nmesh]->index1_ = index1_;
					_pindex[_nmesh]->index2_ = index3_;
					if(_nmesh ++ == max_nmesh){
						AfxMessageBox(_T("Too many faces"),MB_OK);
						return false;
					}

					_pindex[_nmesh] = new ply_index;
					_pindex[_nmesh]->index0_ = index1_;
					_pindex[_nmesh]->index1_ = index2_;
					_pindex[_nmesh]->index2_ = index3_;
					if(_nmesh ++ == max_nmesh){
						AfxMessageBox(_T("Too many faces"),MB_OK);
						return false;
					}
				}
			}
		}

		if(buf[0] == 'n') {
			_pnormal[_nnormal] = new ply_vertex;
#ifdef PR_DOUBLE_PRECISION
			sscanf_s(buf,"%c %lf %lf %lf",&ch,
				&_pnormal[_nnormal]->p.elementAt(0),
				&_pnormal[_nnormal]->p.elementAt(1),
				&_pnormal[_nnormal]->p.elementAt(2));
#else
			sscanf_s(buf,"%c %f %f %f",&ch,
				&_pnormal[_nnormal]->p.elementAt(0),
				&_pnormal[_nnormal]->p.elementAt(1),
				&_pnormal[_nnormal]->p.elementAt(2));
#endif
			if(_nnormal ++ == max_nmesh){
				AfxMessageBox(_T("Too many normals"),MB_OK);
				return false;
			}
		}
	}

	_is.close();

	if(vertex_) delete[] vertex_;
	if(attr_) delete[] attr_;
	if(index_) delete[] index_;
	if(normal_) delete[] normal_;
	if(status_) delete[] status_;
	if(tvertex_) delete[] tvertex_;
	if(tattr_) delete[] tattr_;

	nvertex_ = _nvertex-1;
	nmesh_ = _nmesh;

	vertex_ = new ply_vertex[nvertex_];
	attr_   = new ply_attr[nvertex_];
	index_  = new ply_index[nmesh_];
	normal_ = new ply_vector[nmesh_];
	status_ = new BYTE[nmesh_];
	tvertex_  = NULL;
	tattr_  = NULL;

	for(i = 0; i < nvertex_; ++i){
		vertex_[i].p.elementAt(0) = _pvertex[i+1]->p.elementAt(0);
		vertex_[i].p.elementAt(1) = _pvertex[i+1]->p.elementAt(1);
		vertex_[i].p.elementAt(2) = _pvertex[i+1]->p.elementAt(2);
		attr_[i].confidence_ = 0.0;
		attr_[i].intensity_ = 0.0;
	}
	for(i = 0; i < nmesh_; ++i){
		index_[i].nindex_ = 3;
		index_[i].index0_ = _pindex[i]->index0_ - 1;
		index_[i].index1_ = _pindex[i]->index1_ - 1;
		index_[i].index2_ = _pindex[i]->index2_ - 1;
		status_[i] = SHOW;
	}
	for(i = 0; i < _nnormal; ++i){
		normal_[i].p.elementAt(0) = _pnormal[i]->p.elementAt(0);
		normal_[i].p.elementAt(1) = _pnormal[i]->p.elementAt(1);
		normal_[i].p.elementAt(2) = _pnormal[i]->p.elementAt(2);
	}

	for(i = 0; i < nvertex_; ++i) delete _pvertex[i];
	for(i = 0; i < nmesh_; ++i) delete _pindex[i];
	for(i = 0; i < _nnormal; ++i)	delete _pnormal[i];

	delete[] _pvertex;
	delete[] _pindex;
	delete[] _pnormal;

	for(i=0; i<MAXTEXS; i++) texture_[i] = FALSE;

	CalcCenter();
	CalcScale();
	CalcNormal(CCW);

	return TRUE;
}

BOOL PrPly::LoadPlyFileAsNDT(char* lpszPathName, bool CCW, BOOL CENTERING)
{
	// ファイルのオープン
	char		buf[256];
	int			_nvertex = 0;

	std::ifstream	_is(lpszPathName, std::ios::in | std::ios::_Nocreate | std::ios::binary);

	if(!_is){
		fprintf(stderr,"File open failed");
		return false;
	}

	_is.getline(buf, 256);
	sscanf(buf, "%d", &nvertex_);
	if(vertex_) delete[] vertex_;
	vertex_ = new ply_vertex[nvertex_];

	while(!_is.eof()){
		_is.getline(buf, 256);
#ifdef PR_DOUBLE_PRECISION
		sscanf(buf,"%lf%lf%lf",
			&vertex_[_nvertex].p.elementAt(0),
			&vertex_[_nvertex].p.elementAt(1),
			&vertex_[_nvertex].p.elementAt(2));
#else
		sscanf(buf,"%f%f%f",
			&vertex_[_nvertex].p.elementAt(0),
			&vertex_[_nvertex].p.elementAt(1),
			&vertex_[_nvertex].p.elementAt(2));
#endif
			vertex_[_nvertex].p *= 0.01f;

			if(++_nvertex==nvertex_) break;
	}

	_is.close();

	if(attr_) delete[] attr_;
	if(index_) delete[] index_;
	if(normal_) delete[] normal_;
	if(status_) delete[] status_;
	if(tvertex_) delete[] tvertex_;
	if(tattr_) delete[] tattr_;

	nmesh_ = 0;

	attr_   = new ply_attr[nvertex_];
	index_  = new ply_index[nmesh_];
	normal_ = new ply_vector[nmesh_];
	status_ = new unsigned char[nmesh_];
	tvertex_  = NULL;
	tattr_  = NULL;

	for(int i=0; i<MAXTEXS; i++) texture_[i] = false;

	CalcCenter();
	CalcScale();
	CalcNormal(CCW);
	
	return true;
}

void PrPly::refine()
{
	int i;
	int*		_flags = new int[nvertex_];
	::memset((char*)_flags, 0, sizeof(int) * nvertex_);	
	int			_idx = 0;

	for(i=0;i<nmesh_;i++){
		if(status_[i] != DEL){
				++_flags[index_[i].index0_];
				++_flags[index_[i].index1_];
				++_flags[index_[i].index2_];
				++_idx;
		}
	}

	int		_rvnum = 0;
	for(i = 0; i < nvertex_; ++i){
		if(_flags[i]){
			_flags[i] = _rvnum++;
		}
		else
			_flags[i] = -1;
	}

	ply_vertex*	_vertex = new ply_vertex[_rvnum];
	ply_attr*	_attr   = new ply_attr[_rvnum];
	tex_vertex* _tvertex = NULL;
	if(tvertex_) _tvertex = new tex_vertex[_rvnum];
	int			_vnum = 0;
	for(i = 0; i < nvertex_; ++i){
		if(_flags[i] >= 0){
			(ply_vertex&)(_vertex[_vnum]) = vertex_[i];
			(ply_attr&)(_attr[_vnum]) = attr_[i];
			if(tvertex_) (tex_vertex&)(_tvertex[_vnum]) = tvertex_[i];
			_vnum++;
		}
	}

	ply_index*	_index  = new ply_index[_idx];
	ply_vector*	_normal = new ply_vector[_idx];
	BYTE*		_status = new BYTE[_idx];
	tex_attr* _tattr = NULL;
	if(tattr_) _tattr = new tex_attr[_idx];
	int			_inum = 0;
	for(i=0;i<nmesh_;i++){
		if(status_[i] != DEL){
			ply_index&		_pindex = _index[_inum];
			ply_index&		_oindex = index_[i];
			_pindex.nindex_ = _oindex.nindex_;
			_pindex.index0_ = _flags[_oindex.index0_];
			_pindex.index1_ = _flags[_oindex.index1_];
			_pindex.index2_ = _flags[_oindex.index2_];

			ply_vector&		_pnormal = _normal[_inum];
			ply_vector&		_onormal = normal_[i];
			_pnormal = _onormal;

			BYTE&		_pstatus = _status[_inum];
			BYTE&		_ostatus = status_[i];
			_pstatus = _ostatus;

			if(tattr_) (tex_attr&)(_tattr[_inum]) = tattr_[i];

			_inum++;
		}
	}
	delete []	_flags;

	delete [] vertex_;
	delete [] attr_;
	delete [] index_;
	delete [] normal_;
	delete [] status_;
	if(tvertex_) delete [] tvertex_;
	if(tattr_) delete [] tattr_;

	nvertex_ = _rvnum;
	nmesh_ = _idx;
	vertex_ = _vertex;
	attr_ = _attr;
	index_ = _index;
	normal_ = _normal;
	status_ = _status;
	tvertex_ = _tvertex;
	tattr_ = _tattr;

	CalcCenter();
	CalcScale();

}

BOOL PrPly::SavePlyFile(char* lpszPathName, int type, bool COLOR, bool MATRIX, bool CCW, int SAVECOLORTYPE)
{
	int i;
	if(vertex_==NULL) return FALSE;

	std::ofstream	_os(lpszPathName, std::ios::out | std::ios::binary);

	if(!_os){
		AfxMessageBox(_T("File cannot open"),MB_OK);
		return FALSE;
	}
	
	_os << "ply\n";
 
	if(type == BINARY)
		_os << "format binary_little_endian 1.0\n";
	else 
		_os << "format ascii 1.0\n";

	if(TransformMatrix){
		_os << "comment with matrix" << std::endl;
		if(MATRIX){
			_os << "matrix " << R.elementAt(0,0) << " " << R.elementAt(1,0) << " " << R.elementAt(2,0) << " 0" << std::endl;
			_os << "matrix " << R.elementAt(0,1) << " " << R.elementAt(1,1) << " " << R.elementAt(2,1) << " 0" << std::endl;
			_os << "matrix " << R.elementAt(0,2) << " " << R.elementAt(1,2) << " " << R.elementAt(2,2) << " 0" << std::endl;
			_os << "matrix " << T.elementAt(0)   << " " << T.elementAt(1)   << " " << T.elementAt(2)   << " 1" << std::endl;
		} else {
			_os << "comment matrix " << R.elementAt(0,0) << " " << R.elementAt(1,0) << " " << R.elementAt(2,0) << " 0" << std::endl;
			_os << "comment matrix " << R.elementAt(0,1) << " " << R.elementAt(1,1) << " " << R.elementAt(2,1) << " 0" << std::endl;
			_os << "comment matrix " << R.elementAt(0,2) << " " << R.elementAt(1,2) << " " << R.elementAt(2,2) << " 0" << std::endl;
			_os << "comment matrix " << T.elementAt(0)   << " " << T.elementAt(1)   << " " << T.elementAt(2)   << " 1" << std::endl;
		}
	}

	if(IntrinsicMatrix){
		_os << "comment with intrinsic matrix" << std::endl;
		_os << "comment " <<  A.elementAt(0,0) << " " <<  A.elementAt(0,1) << " " <<  A.elementAt(0,2) << std::endl;
		_os << "comment " <<  A.elementAt(1,0) << " " <<  A.elementAt(1,1) << " " <<  A.elementAt(1,2) << std::endl;
		_os << "comment " <<  A.elementAt(2,0) << " " <<  A.elementAt(2,1) << " " <<  A.elementAt(2,2) << std::endl;
	}

	_os << "element vertex " << nvertex_ << '\n';
	_os << "property float x\n";
	_os << "property float y\n";
	_os << "property float z\n";
	_os << "property float confidence\n";
	if(COLOR){
		switch(SAVECOLORTYPE){
		case MY_RGBA:
			_os << "property uchar red\n";
			_os << "property uchar green\n";
			_os << "property uchar blue\n";
			_os << "property uchar alpha\n";
			break;
		case MY_BGRA:
			_os << "property uchar blue\n";
			_os << "property uchar green\n";
			_os << "property uchar red\n";
			_os << "property uchar alpha\n";
			break;
		}
	} else {
		_os << "property float intensity\n";
	}
	_os << "element face " << nmesh_ << '\n';
	_os << "property list uchar int vertex_indices\n";
	_os << "end_header" << std::endl;

	int _nv(0), _nm(0);

	if(type == BINARY){
		for(i = 0; i < nvertex_; ++i){
			if(!_os.good()) break;
			float vinfo[5];
			vinfo[0] = (float)vertex_[i].p.elementAt(0);
			vinfo[1] = (float)vertex_[i].p.elementAt(1);
			vinfo[2] = (float)vertex_[i].p.elementAt(2);
			vinfo[3] = (float)attr_[i].confidence_;
			if(COLOR) {
				BYTE* c = (BYTE*)(vinfo+4);
				switch(SAVECOLORTYPE){
				case MY_RGBA:
					*(c+0) = attr_[i].color_.red;
					*(c+1) = attr_[i].color_.green;
					*(c+2) = attr_[i].color_.blue;
					*(c+3) = attr_[i].color_.alpha;
					break;
				case MY_BGRA:
					*(c+0) = attr_[i].color_.blue;
					*(c+1) = attr_[i].color_.green;
					*(c+2) = attr_[i].color_.red;
					*(c+3) = attr_[i].color_.alpha;
					break;
				}
			} else vinfo[4] = (float)attr_[i].intensity_;

			_os.write((char*)&vinfo,sizeof(vinfo));
			_nv++;
		}
		for(i = 0; i < nmesh_; ++i){
			if(!_os.good()) break;
			_os.write((char*)&index_[i].nindex_,1);
//			_os.write("3",1);
			_os.write((char*)&index_[i].index0_,12);
			_nm++;
		}
	} else {
		for(i = 0; i < nvertex_; ++i){
			if(!_os.good()) break;
			_os << vertex_[i].p.elementAt(0) << " ";
			_os << vertex_[i].p.elementAt(1) << " ";
			_os << vertex_[i].p.elementAt(2) << " ";
			_os << attr_[i].confidence_ << " ";

			if(COLOR) {
				switch(SAVECOLORTYPE){
				case MY_RGBA:
					_os << (int)attr_[i].color_.red << " ";
					_os << (int)attr_[i].color_.green << " ";
					_os << (int)attr_[i].color_.blue << " ";
					_os << (int)attr_[i].color_.alpha << std::endl;
					break;
				case MY_BGRA:
					_os << (int)attr_[i].color_.blue << " ";
					_os << (int)attr_[i].color_.green << " ";
					_os << (int)attr_[i].color_.red << " ";
					_os << (int)attr_[i].color_.alpha << std::endl;
					break;
				}
			} else _os << attr_[i].intensity_ << std::endl;
			_nv++;
		}
		for(i = 0; i < nmesh_; ++i){
			if(!_os.good()) break;
//			_os << static_cast<int>(index_[i].nindex_) << " ";
			_os << "3 ";
//			_os << index_[i].nindex_ << " ";

			_os << index_[i].index0_ << " ";
			_os << index_[i].index1_ << " ";
			_os << index_[i].index2_ << '\n';
			_nm++;
		}
	}
	_os.close();

	if(nvertex_ != _nv || nmesh_ != _nm){
		CString str;
		str.Format(_T("This file is broken\nNumber of vertex or mesh is wrong\nvertex %d (%d)  mesh %d (%d)"),_nv,nvertex_,_nm,nmesh_);
		AfxMessageBox(str,MB_OK);
		return FALSE;
	}

	return TRUE;
}

BOOL PrPly::SavePlyFileAsVRML(char* lpszPathName, bool CCW)
{
	int i;
	if(vertex_==NULL) return FALSE;
	
	std::ofstream _os(lpszPathName, std::ios::out | std::ios::binary);
	
	if(!_os){
		AfxMessageBox(_T("File cannot open"),MB_OK);
		return FALSE;
	}
	
	_os << _T("#VRML V2.0 utf8") << std::endl;
	_os << _T("Group {") << std::endl;
	_os << _T("  children [") << std::endl;
	_os << _T("    Viewpoint {") << std::endl;
	_os << _T("      position      0 0 20") << std::endl;
	_os << _T("      orientation   1  0  0  0") << std::endl;
	_os << _T("    }") << std::endl;
	_os << _T("    Transform {") << std::endl;
	_os << _T("      children [") << std::endl;
	_os << _T("        Shape {") << std::endl;
	_os << _T("          appearance Appearance { material Material {") << std::endl;
	_os << _T("            ambientIntensity  0.5") << std::endl;
	_os << _T("            diffuseColor      0.7  0.7  0.7") << std::endl;
	_os << _T("            emissiveColor     0.13 0.13 0.13") << std::endl;
	_os << _T("            }") << std::endl;
	_os << _T("          }") << std::endl;
	_os << _T("          geometry IndexedFaceSet {") << std::endl;
	if(CCW)
		_os << _T("            ccw TRUE") << std::endl;
	else
		_os << _T("            ccw FALSE") << std::endl;
	_os << _T("            coord Coordinate {") << std::endl;
	_os << _T("            point [  ") << std::endl;
	
	for(i = 0; i < nvertex_; ++i){
		_os << _T("                       ");
		_os << vertex_[i].p.elementAt(0) << _T(" ");
		_os << vertex_[i].p.elementAt(1) << _T(" ");
		_os << vertex_[i].p.elementAt(2);
		if(i!=nvertex_-1) _os << _T(",");
		_os << std::endl;
	}
	_os << _T("              ]") << std::endl;
	_os << _T("            }") << std::endl;
	_os << _T("           coordIndex [ ") << std::endl;
	
	for(i = 0; i < nmesh_; ++i){
		_os << _T("                       ");
		_os << index_[i].index0_  << _T(" ");
		_os << index_[i].index1_  << _T(" ");
		_os << index_[i].index2_  << _T(" ");
		_os << _T(" -1");
		if(i!=nmesh_-1) _os << _T(",");
		_os << std::endl;
	}
	_os << _T("              ]") << std::endl;
	
	// Doi added the next line 011206
	_os << _T("            solid FALSE") << std::endl;
	
	// Doi added the next block 011130
	
	int count = 0;
	std::multimap<float,int> color_map;
	
	_os << _T("           color Color {") << std::endl;
	_os << _T("              color[") << std::endl;
	
	bool flg=false;
	for( i = 0 ; i < nvertex_ ; i++ ){
		if( color_map.find( attr_[i].intensity_ ) == color_map.end() ){
			if( !color_map.empty()  && flg )
				_os << _T(" , ") << std::endl;
			flg=true;
			std::pair<float,int> p( attr_[i].intensity_ , count++ );
			color_map.insert( p );
			float r , g , b;
			unsigned int t = *(unsigned int*)(&(attr_[i].intensity_));
			r = ( ( t << 24 ) >> 24 ) / 256.0;
			g = ( ( t << 16 ) >> 24 ) / 256.0;
			b = ( ( t << 8  ) >> 24 ) / 256.0;
			_os << _T("                ") << r << _T(" ") << g << _T(" ") << b;
		}
	}
	
	_os << _T("                ]") << std::endl;
	_os << _T("              }") << std::endl;
	
	_os << _T("           colorIndex [ ") << std::endl;
	
	for( i = 0; i < nmesh_ ; i++ ){
		std::multimap<float,int>::iterator itr;
		_os << _T("           ");
		itr = color_map.find( attr_[index_[i].index0_].intensity_ );
		if( itr != color_map.end() ){
			_os << (*itr).second << _T(" ");
		} else {
			_os << 0 << _T(" ");
		}
		itr = color_map.find( attr_[index_[i].index1_].intensity_ );
		if( itr != color_map.end() ){
			_os << (*itr).second << _T(" ");
		} else {
			_os << 0 << _T(" ");
		}
		itr = color_map.find( attr_[index_[i].index2_].intensity_ );
		if( itr != color_map.end() ){
			_os << (*itr).second << _T(" ");
		} else {
			_os << 0 << _T(" ");
		}
		if( i == nmesh_ - 1 )
			_os << -1 << std::endl;
		else
			_os << -1 << _T(",")  << std::endl;
	}
	
	_os << _T("              ]") << std::endl;
	
	// End Doi

	_os << _T("   }") << std::endl;
	_os << _T(" }") << std::endl;
	
	_os << _T("       ]") << std::endl;
	_os << _T("    }") << std::endl;
	_os << _T("  ]") << std::endl;
	_os << _T("}") << std::endl;
	
	_os.close();
	
	return TRUE;
}


//BOOL PrPly::SavePlyFileAsVRMLWithGifImage(char* lpszPathName, bool CW, CString* _bmpfname)
//{
//	int i,j;
//	if(vertex_==NULL) return FALSE;
//
//	std::ofstream	_os(lpszPathName, std::ios::out | std::ios::binary);
//
//	if(!_os){
//		AfxMessageBox(_T("File cannot open"),MB_OK);
//		return FALSE;
//	}
//
//	_os << _T("#VRML V2.0 utf8") << std::endl;
//	_os << _T("Group {") << std::endl;
//	_os << _T("  children [") << std::endl;
//	_os << _T("    Viewpoint {") << std::endl;
//	_os << _T("      position      0 0 20") << std::endl;
//	_os << _T("      orientation   1  0  0  0") << std::endl;
//	_os << _T("    }") << std::endl;
//	_os << _T("    Transform {") << std::endl;
//	_os << _T("      children [") << std::endl;
//	for(int t=0; t<MAXTEXS; t++){
//		if(texture_[t]){
//			PrPly _ply(*this, t);
//			CString _fname = _bmpfname[t];
//			_fname.Replace(_T(".bmp"),_T(".gif"));
//			_os << _T("        Shape {") << std::endl;
//			_os << _T("          appearance Appearance { material Material {") << std::endl;
//			_os << _T("            ambientIntensity  0.5") << std::endl;
//			_os << _T("            diffuseColor      0.7  0.7  0.7") << std::endl;
//			_os << _T("            emissiveColor     0.13 0.13 0.13") << std::endl;
//			_os << _T("            }") << std::endl;
//			_os << _T("            texture DEF _0 ImageTexture {") << std::endl;
//			_os << _T("              url \"./") << _fname << _T("\"") << std::endl;
//			_os << _T("            }") << std::endl;
//			_os << _T("          }") << std::endl;
//			_os << _T("          geometry IndexedFaceSet {") << std::endl;
//			if(!CW)
//				_os << _T("            ccw FALSE") << std::endl;
//			else
//				_os << _T("            ccw TRUE") << std::endl;
//			_os << _T("            coord Coordinate {") << std::endl;
//			_os << _T("            point [  ") << std::endl;
//
//			PrVector3 p;
//			for(i = 0; i < _ply.nvertex_; ++i){
//				if(TransformMatrix){
//					p = R * _ply.vertex_[i].p + T;
//					_os << "                       ";
//					_os << p.elementAt(0) << " ";
//					_os << p.elementAt(1) << " ";
//					_os << p.elementAt(2);
//					if(i!=_ply.nvertex_-1) _os << ",";
//					_os << std::endl;
//				} else {
//					_os << "                       ";
//					_os << _ply.vertex_[i].p.elementAt(0) << " ";
//					_os << _ply.vertex_[i].p.elementAt(1) << " ";
//					_os << _ply.vertex_[i].p.elementAt(2);
//					if(i!=_ply.nvertex_-1) _os << ",";
//					_os << std::endl;
//				}
//			}
//
//			_os << _T("              ]") << std::endl;
//			_os << _T("            }") << std::endl;
//			_os << _T("           coordIndex [ ") << std::endl;
//
//			for(i = 0; i < _ply.nmesh_; ++i){
//				_os << _T("                       ");
//				_os << _ply.index_[i].index0_  << _T(" ");
//				_os << _ply.index_[i].index1_  << _T(" ");
//				_os << _ply.index_[i].index2_  << _T(" ");
//				_os << _T(" -1");
//				if(i!=_ply.nmesh_-1) _os << _T(",");
//				_os << std::endl;
//			}
//  			_os << _T("              ]") << std::endl;
//   			_os << _T(" texCoord TextureCoordinate {") << std::endl;
//			_os << _T("              point [") << std::endl;
//			for(i = 0; i < _ply.nvertex_; ++i){
//				_os << _T("                       ");
//				_os << _ply.tvertex_[i].u[0] << _T(" ");
//				_os << _ply.tvertex_[i].v[0] << _T(" ");
//				if(i!=_ply.nvertex_-1) _os << _T(",");
//				_os << std::endl;
//			}
//			_os << _T("              ]") << std::endl;
//			_os << _T("	    }") << std::endl;
//			_os << _T("	  }") << std::endl;
//			_os << _T("	}") << std::endl;
//		}
//	}
//	_os << _T("       ]") << std::endl;
//	_os << _T("    }") << std::endl;
//	_os << _T("  ]") << std::endl;
//	_os << _T("}") << std::endl;
//
//	_os.close();
//
//	return TRUE;
//}

//BOOL PrPly::SavePlyFileAsOBJ(char* lpszPathName, bool COLOR, bool CCW)
//{
//	int i;
//	if(vertex_==NULL) return FALSE;
//
//	std::ofstream	_os(lpszPathName, std::ios::out | std::ios::binary);
//
//	CString lpszPathColorName(lpszPathName);
//	lpszPathColorName.Replace(_T(".obj"),_T(".mtl"));
//	std::ofstream	_osc(lpszPathColorName, std::ios::out | std::ios::binary);
//
//	if(!_os){
//		AfxMessageBox(_T("File cannot open"),MB_OK);
//		return FALSE;
//	}
//
//	_os << _T("g object") << std::endl;
//	_os << _T("mtllib ") << lpszPathColorName << std::endl;
//
//	PrVector3 p;
//	for(i = 0; i < nvertex_; ++i){
//		_os << _T("v ");
//		_os << vertex_[i].p.elementAt(0) << _T(" ");
//		_os << vertex_[i].p.elementAt(1) << _T(" ");
//		_os << vertex_[i].p.elementAt(2) << std::endl;
//	}
//
//	_os << std::endl;
//
//	CString cn;
//	BIT32 RGBA0, RGBA1, RGBA2;
//	Float Red, Green, Blue, Intensity;
//	for(i = 0; i < nmesh_; ++i){
//		cn.Format(_T("c%d"),i);
//		_os << _T("usemtl ") << cn << std::endl;
//
//		_osc << _T("newmtl ") << cn << std::endl;
//		if(COLOR){
//			RGBA0 = *(BIT32*)&attr_[index_[i].index0_].intensity_;
//			RGBA1 = *(BIT32*)&attr_[index_[i].index1_].intensity_;
//			RGBA2 = *(BIT32*)&attr_[index_[i].index2_].intensity_;
//			Red = ((Float)RGBA0.red + (Float)RGBA1.red + (Float)RGBA2.red) / 3.0 / 256.0;
//			Green = ((Float)RGBA0.green + (Float)RGBA1.green + (Float)RGBA2.green) / 3.0 / 256.0;
//			Blue = ((Float)RGBA0.blue + (Float)RGBA1.blue + (Float)RGBA2.blue) / 3.0 / 256.0;
//			_osc << _T("Kd ") << Red << _T(" ") << Green << _T(" ") << Blue << std::endl;
//			_osc << std::endl;
//		} else {
//			Intensity = attr_[index_[i].index0_].intensity_ + attr_[index_[i].index0_].intensity_ + attr_[index_[i].index0_].intensity_;
//			Intensity /= (3.0*256.0);
//			_osc << _T("Kd ") << Intensity << _T(" ") << Intensity << _T(" ") << Intensity << std::endl;
//			_osc << std::endl;
//		}
//		_os << _T("f ");
//		_os << index_[i].index0_+1  << _T(" ");
//		_os << index_[i].index1_+1  << _T(" ");
//		_os << index_[i].index2_+1  << std::endl;
//	}
//
//	_os.close();
//	_osc.close();
//
//	return TRUE;
//}
//
//BOOL PrPly::SavePlyFileAsSMF(char* lpszPathName, bool CCW)
///* this format is used in qslim */
//{
//	int i;
//	if(vertex_==NULL) return FALSE;
//
//	std::ofstream	_os(lpszPathName, std::ios::out | std::ios::binary);
//
//	if(!_os){
//		AfxMessageBox(_T("File cannot open"),MB_OK);
//		return FALSE;
//	}
//
//	_os << _T("#SMF2.0") << std::endl;
//	_os << _T("# First, define the geometry.") << std::endl;
//	_os << _T("#") << std::endl;
//	PrVector3 p;
//	for(i = 0; i < nvertex_; ++i){
//		_os << _T("v ");
//		_os << vertex_[i].p.elementAt(0) << _T(" ");
//		_os << vertex_[i].p.elementAt(1) << _T(" ");
//		_os << vertex_[i].p.elementAt(2) << std::endl;
//	}
//
//	for(i = 0; i < nmesh_; ++i){
//		_os << _T("f ");
//		_os << index_[i].index0_+1  << _T(" ");
//		_os << index_[i].index1_+1  << _T(" ");
//		_os << index_[i].index2_+1  << std::endl;
//	}
///*
//	_os << "#" << endl;
//	_os << "#  Colors will be bound 1 per face." << endl;
//	_os << "#" << endl;
//	_os << "bind c face" << endl;
//	for(i = 0; i < nvertex_; ++i){
//		_os << "c ";
//		_os << "1.0 0.0 0.0" << endl;
//	}
//*/
//	_os << _T("#") << std::endl;
//	_os << _T("#  Normal vectors will be bound 1 per face.") << std::endl;
//	_os << _T("#") << std::endl;
//	_os << _T("bind n face") << std::endl;
//	for(i = 0; i < nvertex_; ++i){
//		_os << _T("n ");
//		_os << normal_[i].p.elementAt(0) << _T(" ");
//		_os << normal_[i].p.elementAt(1) << _T(" ");
//		_os << normal_[i].p.elementAt(2) << std::endl;
//	}
//
//	_os.close();
//
//	return TRUE;
//}

void PrPly::CalcCenter()
{
	PrVector3 sum;
	for(int i = 0; i < nvertex_; ++i) sum += vertex_[i].p;
	sum *= (1.0/(Float)nvertex_);
	center_ = sum;
}

void PrPly::CalcAverage()
{
	avg = 0;
	for(int i = 0; i < nmesh_; ++i){
		avg += (vertex_[index_[i].index0_].p-vertex_[index_[i].index1_].p).magnitude();
		avg += (vertex_[index_[i].index1_].p-vertex_[index_[i].index2_].p).magnitude();
		avg += (vertex_[index_[i].index2_].p-vertex_[index_[i].index0_].p).magnitude();
	}
	avg /= (3.0*(Float)nmesh_);
}

void PrPly::SetShowAll()
{
	for(int i = 0; i < nmesh_; ++i){
		status_[i] = SHOW;
	}
}

void PrPly::CalcScale()
{
	int i;
	
	maxx = maxy = maxz = -MAXFLOAT;
	minx = miny = minz = MAXFLOAT;
	for(i = 0; i < nvertex_; ++i) {
		maxx = max(maxx,vertex_[i].p[0]);
		maxy = max(maxy,vertex_[i].p[1]);
		maxz = max(maxz,vertex_[i].p[2]);
 		minx = min(minx,vertex_[i].p[0]);
		miny = min(miny,vertex_[i].p[1]);
		minz = min(minz,vertex_[i].p[2]);
	}
}

void PrPly::CalcSurfaceArea()
{
	PrVector3 v1,v2;
	area = 0;
	for(int i = 0; i < nmesh_; ++i){
		v1 = vertex_[index_[i].index0_].p-vertex_[index_[i].index1_].p;
		v2 = vertex_[index_[i].index0_].p-vertex_[index_[i].index2_].p;
		area += (v1.cross(v2)).magnitude()/2.0;
	}
}

void PrPly::CalcVolume()
{
	PrVector3 v1,v2,v3,n;
	volume = 0;
	for(int i = 0; i < nmesh_; ++i){
		v1 = vertex_[index_[i].index0_].p-vertex_[index_[i].index1_].p;
		v2 = vertex_[index_[i].index0_].p-vertex_[index_[i].index2_].p;
		n = v1.cross(v2);
		v3 = center_ - vertex_[index_[i].index0_].p;
		volume += fabs(n.dot(v3))/6.0;
	}
}

void PrPly::CalcNormal(bool CCW)
{
	PrVector3 v1, v2;
	for(int i=0; i<nmesh_; i++){
		v1 = vertex_[index_[i].index1_].p - vertex_[index_[i].index0_].p;
		v2 = vertex_[index_[i].index2_].p - vertex_[index_[i].index0_].p;
		if(CCW)	normal_[i].p = v1.cross(v2);
		else normal_[i].p = v2.cross(v1);
		normal_[i].p.normalize();
	}
}

PrPly &PrPly::operator=(const PrPly &sPly)
{
	if (this != &sPly){
		int i, j;
		if(vertex_) delete[] vertex_;
		if(attr_) delete[] attr_;
		if(index_) delete[] index_;
		if(normal_) delete[] normal_;
		if(status_) delete[] status_;
		if(tvertex_) delete[] tvertex_;
		if(tattr_) delete[] tattr_;

		nvertex_ = sPly.nvertex_;
		nmesh_ = sPly.nmesh_;

		vertex_ = new ply_vertex[nvertex_];
		attr_ = new ply_attr[nvertex_];
		index_ = new ply_index[nmesh_];
		normal_ = new ply_vector[nmesh_];
		status_ = new BYTE[nmesh_];
		if(sPly.tvertex_) tvertex_ = new tex_vertex[nvertex_];
		else tvertex_ = NULL;
		if(sPly.tattr_) tattr_ = new tex_attr[nmesh_];
		else tattr_ = NULL;

		for(i = 0; i < nvertex_; ++i){
			vertex_[i].p = sPly.vertex_[i].p;
			attr_[i].confidence_ = sPly.attr_[i].confidence_;
			attr_[i].intensity_  = sPly.attr_[i].intensity_;
			if(tvertex_){
				for(j = 0; j < MAXTEXS; ++j){
					tvertex_[i].u[j] = sPly.tvertex_[i].u[j];
					tvertex_[i].v[j] = sPly.tvertex_[i].v[j];
				}
			}
		}
		for(i = 0; i < nmesh_; ++i){
			index_[i].nindex_ = sPly.index_[i].nindex_;
			index_[i].index0_ = sPly.index_[i].index0_;
			index_[i].index1_ = sPly.index_[i].index1_;
			index_[i].index2_ = sPly.index_[i].index2_;
			normal_[i].p = sPly.normal_[i].p;
			status_[i] = sPly.status_[i];
			if(tattr_){
				for(j = 0; j < MAXTEXS; ++j){
					tattr_[i].number[j] = sPly.tattr_[i].number[j];
					tattr_[i].direction[j] = sPly.tattr_[i].direction[j];
					tattr_[i].depth[j] = sPly.tattr_[i].depth[j];
				}
			}
		}

		for(i=0; i<MAXTEXS; i++) texture_[i] = sPly.texture_[i];

		TransformMatrix = sPly.TransformMatrix;
		R = sPly.R;
		T = sPly.T;

	}

	return (*this);
}

//BOOL PrPly::ConvertToBitmap(PrBitmap& sBitmap, PrVector3 p, PrQuaternion q, PrVector3 center)
//{
//	PrVector3 po, pr, pn;
//	DWORD offset_w, offset_h, offset;
//	int i,j,k;
//	int interval = 10;
//	Float MtoMM = 1000.0f;
//
//	Float resolution = 300.0; // pixel per inch
//	Float inch2mm = 1.0/25.4; // inch per mm
//	Float focus = 70.0; // mm
//	Float xoffset = 2000.0/4.0/2.0;
//	Float yoffset = 1312.0/4.0/2.0;
//	PrMatrix3 Ri(-focus * resolution * inch2mm, 0.0, xoffset, 0.0, -focus * resolution * inch2mm, yoffset, 0.0, 0.0, 1.0);
//	PrMatrix3 Rm = q.matrix();
//	PrMatrix3 R  = Ri * Rm;
//	PrVector3 T = Ri * p;
//
//	DWORD width = sBitmap.GetWidth();
//	DWORD height = sBitmap.GetHeight();
//	WORD biBitCount = sBitmap.GetbiBitCount();
//
//	::memset((char*)sBitmap.m_BmpImage, 0xff, sBitmap.m_BmpInfo->bmiHeader.biSizeImage);
//
//	for(i=0;i<nmesh_;i+=interval){
//		if(status_[i] == SHOW){
//			for(j=0;j<3;j++){
//				switch(j){
//				case 0:
//					k = index_[i].index0_;
//					break;
//				case 1:
//					k = index_[i].index1_;
//					break;
//				case 2:
//					k = index_[i].index2_;
//					break;
//				}
//
//				po = vertex_[k].p * MtoMM - center;
////				p.values(-3535.6, -3731.1, -4296.0);
//				pn = R * po + T;
//				pr[0] = pn[0] / pn[2];
//				pr[1] = pn[1] / pn[2];
//				pr[2] = 1.0;
//				offset_w = (int)(pr[0]);
//				offset_h = (int)(pr[1]);
//				offset = (offset_w + (height - offset_h - 1) * width ) * (biBitCount/8);
//				if(offset < sBitmap.m_BmpInfo->bmiHeader.biSizeImage && 
//					offset_w > 0 && offset_w < width && offset_h > 0 && offset_h < height){
//						switch(biBitCount){
//							case 32:
//							case 24:
//								*(char*)(sBitmap.m_BmpImage + offset) = (char)0x80;
//								*(char*)(sBitmap.m_BmpImage + offset+1) = (char)0x00;
//								*(char*)(sBitmap.m_BmpImage + offset+2) = (char)0x00;
//								break;
//							case 16:
//								*(BIT16*)(sBitmap.m_BmpImage + offset) = (BIT16)0x8000;
//								break;
//							default:
//								*(char*)(sBitmap.m_BmpImage + offset) = (char)0x80;
//						}
//				}
//			}
//		}
//	}
//	return TRUE;
//}

void PrPly::CalcReflectanceEdges(Float thresh, bool CYRACGP)
{
	int i;
	ply_attr* _attr = new ply_attr[nvertex_];

	for(i = 0; i < nvertex_; ++i) _attr[i].intensity_ = _attr[i].confidence_ = 1.0;

	if(CYRACGP){
		for(i = 0; i < nmesh_; ++i){
			if(fabs(attr_[index_[i].index0_].confidence_ - attr_[index_[i].index1_].confidence_) > thresh){
				_attr[index_[i].index0_].intensity_ = _attr[index_[i].index0_].confidence_ = 0.0;
				_attr[index_[i].index1_].intensity_ = _attr[index_[i].index1_].confidence_ = 0.0;
			}
			if(fabs(attr_[index_[i].index1_].confidence_ - attr_[index_[i].index2_].confidence_) > thresh){
				_attr[index_[i].index1_].intensity_ = _attr[index_[i].index1_].confidence_ = 0.0;
				_attr[index_[i].index2_].intensity_ = _attr[index_[i].index2_].confidence_ = 0.0;
			}
			if(fabs(attr_[index_[i].index2_].confidence_ - attr_[index_[i].index0_].confidence_) > thresh){
				_attr[index_[i].index2_].intensity_ = _attr[index_[i].index2_].confidence_ = 0.0;
				_attr[index_[i].index0_].intensity_ = _attr[index_[i].index0_].confidence_ = 0.0;
			}
		}
	} else {
		for(i = 0; i < nmesh_; ++i){
			if(fabs(attr_[index_[i].index0_].intensity_ - attr_[index_[i].index1_].intensity_) > thresh){
				_attr[index_[i].index0_].intensity_ = _attr[index_[i].index0_].confidence_ = 0.0;
				_attr[index_[i].index1_].intensity_ = _attr[index_[i].index1_].confidence_ = 0.0;
			}
			if(fabs(attr_[index_[i].index1_].intensity_ - attr_[index_[i].index2_].intensity_) > thresh){
				_attr[index_[i].index1_].intensity_ = _attr[index_[i].index1_].confidence_ = 0.0;
				_attr[index_[i].index2_].intensity_ = _attr[index_[i].index2_].confidence_ = 0.0;
			}
			if(fabs(attr_[index_[i].index2_].intensity_ - attr_[index_[i].index0_].intensity_) > thresh){
				_attr[index_[i].index2_].intensity_ = _attr[index_[i].index2_].confidence_ = 0.0;
				_attr[index_[i].index0_].intensity_ = _attr[index_[i].index0_].confidence_ = 0.0;
			}
		}
	}

/*
	for(i = 0; i < nvertex_; ++i) {
		_attr[i].confidence_ = 1.0;
		_attr[i].intensity_ = attr_[i].intensity_;
	}

		for(i = 0; i < nmesh_; ++i){
			if(fabs(attr_[index_[i].index0_].intensity_ - attr_[index_[i].index1_].intensity_) > thresh){
				_attr[index_[i].index0_].confidence_ = 0.0;
				_attr[index_[i].index1_].confidence_ = 0.0;
			}
			if(fabs(attr_[index_[i].index1_].intensity_ - attr_[index_[i].index2_].intensity_) > thresh){
				_attr[index_[i].index1_].confidence_ = 0.0;
				_attr[index_[i].index2_].confidence_ = 0.0;
			}
			if(fabs(attr_[index_[i].index2_].intensity_ - attr_[index_[i].index0_].intensity_) > thresh){
				_attr[index_[i].index2_].confidence_ = 0.0;
				_attr[index_[i].index0_].confidence_ = 0.0;
			}
		}
*/
	delete [] attr_;
	attr_ = _attr;
}

void PrPly::CalcTexturePoint(int w, int h, int nm, PrTransform p, PrVector3 c, Float focus, Float resolution, bool CCW)
{
	int i,j;
	Float xoffset = w/2.0f;
	Float yoffset = h/2.0f;
	// focusは負、画像平面がz<0の位置にあるから
	PrMatrix3 Ri(-focus * resolution, 0.0, xoffset, 0.0, -focus * resolution, yoffset, 0.0, 0.0, 1.0);
	PrMatrix3 Rm = p.rotation().matrix();
	PrVector3 t  = p.translation();
	Float u,v;

	PrVector3 po, pr[3], pn[3];
	PrVector3 n, e;

	if(!tvertex_) tvertex_ = new tex_vertex[nvertex_];
	if(!tattr_) tattr_ = new tex_attr[nmesh_];

	for(i = 0; i < nmesh_; ++i){
		po = vertex_[index_[i].index0_].p - c;
		pn[0] = Rm * po + t;
		if(CCW) {
			po = vertex_[index_[i].index1_].p - c;
			pn[1] = Rm * po + t;
			po = vertex_[index_[i].index2_].p - c;
			pn[2] = Rm * po + t;
		} else {
			po = vertex_[index_[i].index2_].p - c;
			pn[1] = Rm * po + t;
			po = vertex_[index_[i].index1_].p - c;
			pn[2] = Rm * po + t;
		}
		n = (pn[0] - pn[1]).cross(pn[0] - pn[2]);
		e = (pn[0] + pn[1] + pn[2]) * (-1.0/3.0);
		tattr_[i].depth[nm] = e.magnitude();
		n.normalize();
		e.normalize();
		tattr_[i].direction[nm] = e.dot(n);

		pn[0] = Ri * pn[0];
		pn[1] = Ri * pn[1];
		pn[2] = Ri * pn[2];

		tattr_[i].number[nm] = TRUE;

		u = (Float) pn[0][0] / pn[0][2] / (Float) w;
		v = (Float) pn[0][1] / pn[0][2] / (Float) h;		

		u = min(max(u, 0.0),1.0);
		v = min(max(v, 0.0),1.0);
		tvertex_[index_[i].index0_].u[nm] = u;
		tvertex_[index_[i].index0_].v[nm] = v;
		
		u = (Float) pn[1][0] / pn[1][2] / (Float) w;
		v = (Float) pn[1][1] / pn[1][2] / (Float) h;		
		
		u = min(max(u, 0.0),1.0);
		v = min(max(v, 0.0),1.0);
		tvertex_[index_[i].index1_].u[nm] = u;
		tvertex_[index_[i].index1_].v[nm] = v;
		
		u = (Float) pn[2][0] / pn[2][2] / (Float) w;
		v = (Float) pn[2][1] / pn[2][2] / (Float) h;	
		
		u = min(max(u, 0.0),1.0);
		v = min(max(v, 0.0),1.0);
		tvertex_[index_[i].index2_].u[nm] = u;
		tvertex_[index_[i].index2_].v[nm] = v;

	}
	texture_[nm] = TRUE;
}
