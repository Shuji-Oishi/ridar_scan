/*
 *      PLY Header File
 *
 *      Shuji Oishi     oishi@cs.tut.ac.jp
 *      Toyohashi University of Technology
 *      2017.07.06
 */

#include <assert.h>

enum VSTATUS {SHOW, HIDE, DEL};
enum PLYTYPE {ASCII, BINARY};
enum RBGTYPE { MY_RGBA, MY_BGRA };

typedef	struct{
	PrVector3 p;
} ply_vector;

typedef struct{
	PrVector3	p;
} ply_vertex;

typedef struct{
	Float	confidence_;
	union {
		struct    
		{ 
			BYTE red;
			BYTE green;
			BYTE blue;
			BYTE alpha;
		} color_;
		Float intensity_;
	};
} ply_attr;

typedef struct{
	char	nindex_;
	int		index0_, index1_, index2_;
} ply_index;

typedef struct{
	Float u[MAXTEXS];
	Float v[MAXTEXS];
} tex_vertex;

typedef struct{
	bool number[MAXTEXS];
	Float direction[MAXTEXS];
	Float depth[MAXTEXS];
} tex_attr;

typedef struct{
	ply_vertex*	vertex_;
	ply_attr*	attr_;
	ply_index*	index_;
	ply_vector*	normal_;
	int			nvertex_;
	int			nmesh_;
	BYTE*		status_;
	PrVector3	center_;
	
	ply_vector	tpos_;
	tex_vertex*	tvertex_;
	tex_attr*	tattr_;
	bool		texture_[MAXTEXS];
} ply_object;

class PrPly : public ply_object
{
protected:

public:
	PrPly();
	PrPly(const PrPly &sPly);
	PrPly(const PrPly &sPly, int t);
	PrPly(int _nvertex, int _nmesh);
	~PrPly();

	BOOL TransformMatrix;
	PrMatrix3 R;
	PrVector3 T;
	Float maxx,maxy,maxz,minx,miny,minz;
	Float avg; //�@���_�Ԃ̕��ϋ���
	Float area; //�@�\�ʐ�
	Float volume; //�@�\�ʐ�
	int COLORTYPE;
	BOOL IntrinsicMatrix;
	PrMatrix3 A;

	PrPly &operator=(const PrPly &sPly);

	BOOL LoadPlyFile(char* lpszPathName, bool CCW = true, bool HEADERONLY = false,  bool RECOVER = false, BOOL CENTERING = false);
	BOOL LoadPlyFileAsSMF(char* lpszPathName, bool CCW = true, BOOL CENTERING = false);
	BOOL LoadPlyFileAsOBJ(char* lpszPathName, bool CCW = true, BOOL CENTERING = false);
	BOOL LoadPlyFileAsNDT(char* lpszPathName, bool CCW = true, BOOL CENTERING = false);
	BOOL SavePlyFile(char* lpszPathName, int type, bool COLOR = false, bool MATRIX = true, bool CCW = true, int SAVECOLORTYPE = MY_RGBA);
	BOOL SavePlyFileAsVRML(char* lpszPathName, bool CCW = true);
//	BOOL SavePlyFileAsVRMLWithGifImage(char* lpszPathName, bool CW, CString* _bmpfname);
//	BOOL SavePlyFileAsOBJ(char* lpszPathName, bool COLOR = false, bool CCW = true);
//	BOOL SavePlyFileAsSMF(char* lpszPathName, bool CCW = true);
	void CalcCenter();
	void CalcScale();
	void CalcAverage();
	void CalcSurfaceArea();
	void CalcVolume();
	void CalcNormal(bool CCW);
	void CalcReflectanceEdges(Float thresh, bool CYRACGP = false);
	void SetShowAll();

//	BOOL ConvertToBitmap(PrBitmap& sBitmap, PrVector3 p, PrQuaternion q, PrVector3 center);
	void refine();
	void PrepareTextureMapping();
	void CalcTexturePoint(int w, int h, int n, PrTransform p, PrVector3 c, Float focus, Float resolution, bool CCW = true);
};
